#Compile in docker
FROM node:12.18.3 AS builder
COPY trade-ui ./trade-ui
WORKDIR /trade-ui
RUN npm i
RUN $(npm bin)/ng build --prod

#Server
FROM nginx:1.15.8-alpine
COPY --from=builder /trade-ui/dist/trade-ui/ /usr/share/nginx/html
