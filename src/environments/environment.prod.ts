export const environment = {
  production: true
};

export const serviceURLs = {
  //Docker
  productService: window.location.href.replace("4200","8111") + "trade",
  yahooApi: window.location.href.replace("4200","8111") + "yahoo"

  //OpenShift
  // productService: window.location.href.replace("trade-ui","trade-rest-api") + "trade",
  // yahooApi: window.location.href.replace("trade-ui","trade-rest-api") + "yahoo"
};
