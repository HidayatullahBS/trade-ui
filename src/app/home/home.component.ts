import { Component, OnInit } from '@angular/core';
import { YahooApiService } from '../yahoo-api.service';
import { Trade } from '../trade';
import { TradesService } from '../trades.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  trendingTickers: any;
  hasTrendingTickers: boolean;
  typeOptions = {
    buy: 'BUY',
    sell: 'SELL'
  };
  loadingGetLive = false;
  loadingCreateTrade = false;

  symbol: string;
  region: string;
  name: string;
  quantity: number;
  price: number;
  type: string;

  constructor(private yahooApiService: YahooApiService, private tradeService: TradesService, private toastr: ToastrService) {
    this.symbol = '';
    this.region = '';
    this.name = '';
    this.price = 0;
    this.type = null;
  }

  ngOnInit(): void {
    this.yahooApiService.getTrendingTickers().subscribe(res => {
      if (res.finance.error == null) {
        this.trendingTickers = res.finance.result[0].quotes;
        this.hasTrendingTickers = true;
      } else {
        this.hasTrendingTickers = false;
      }
    });
  }

  public handleRowClick(event): void {
    const cells = event.target.parentElement.cells;
    this.name = cells[0].innerHTML;
    this.symbol = cells[1].innerHTML;
    this.region = cells[2].innerHTML;
    this.price = cells[3].innerHTML === '-' ? 0 : cells[3].innerHTML;
  }

  public onSubmit(form): void {
    this.loadingCreateTrade = true;
    this.tradeService.createNewTrade(new Trade(-1,
      this.type,
      this.symbol,
      this.quantity,
      this.price,
      'CREATED')
    ).subscribe(
      (response) => {
        this.toastr.success('Successfully created trade. Trade ID: ' + response.tradeId);
        form.resetForm();
        this.loadingCreateTrade = false;
        this.type = null;
      },
      (error) => {
        this.toastr.error('Unable to create trade. Error:\n' + error.error.status + ' ' + error.error.error);
        this.loadingCreateTrade = false;
      }
    );
  }

  public onGetLive(e): void {
    e.preventDefault();
    this.loadingGetLive = true;

    if (!this.yahooApiService.isValidRegion(this.region)) {
      this.loadingGetLive = false;
      this.toastr.error('Invalid region. Please also ensure to use uppercase letters.');
      return;
    }

    this.yahooApiService.getQuotes(this.symbol, this.region).subscribe(res => {
      this.loadingGetLive = false;
      const quote = res.quoteResponse;
      if (quote.error == null) {
        this.name = quote.result[0].shortName;
        this.price = quote.result[0].regularMarketPrice;
      } else {
        this.toastr.error('Unable to retrieve live pricing.');
      }
    });
  }

  public checkSign(num: number): string {
    if (num == null) {
      return '';
    }
    if (Math.sign(num) === -1) {
      return 'red';
    }
    return 'green';
  }
}
