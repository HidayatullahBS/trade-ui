import { Component, OnInit } from '@angular/core';
import { TradesService } from '../trades.service';
import { Observable } from 'rxjs';
import { Trade } from '../trade';

import {ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';


@Component({
  selector: 'app-trades',
  templateUrl: './trades.component.html',
  styleUrls: ['./trades.component.css'],
  providers: [TradesService]
})

export class TradesComponent implements OnInit {

  ELEMENT_DATA: Trade[];
  displayedColumns: string[] = ['tradeId', 'type', 'ticker', 'quantity', 'price', 'state'];
  // @ts-ignore
  dataSource = new MatTableDataSource<Trade>(this.ELEMENT_DATA);
  @ViewChild(MatPaginator, {static : true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private tradeService: TradesService) {
  }
  // trades: Array<Trade>;
  // hasPortfolio: boolean;
  ngOnInit(): void {
    // this.trades = this.tradeService.retrieveAllTradeRecords();
    // this.tradeService.retrieveAllTradeRecords().subscribe(
    //   (response) => {
    //     if (response.length === 0) {
    //       this.hasPortfolio = false;
    //     } else {
    //       this.hasPortfolio = true;
    //       this.trades = response;
    //     }
    //   },
    //   (error) => this.hasPortfolio = false
    // );
    this.getAllTrades();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  // tslint:disable-next-line:typedef
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  // tslint:disable-next-line:typedef
  public getAllTrades(){
    const resp = this.tradeService.retrieveAllTradeRecords();
    resp.subscribe(report => this.dataSource.data = report as Trade[]);
  }


}
