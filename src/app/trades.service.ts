import { Injectable } from '@angular/core';
import { Trade } from './trade';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import { serviceURLs } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TradesService {

  private baseUrl: string = serviceURLs.productService;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private http: HttpClient) { }

  createNewTrade(trade: Trade): Observable<Trade> {
    const serializedForm = JSON.stringify(trade);
    return this.http.post<Trade>(this.baseUrl, serializedForm, this.httpOptions) as Observable<Trade>;
  }

  retrieveAllTradeRecords(): Observable<Array<Trade>>{
    return this.http.get(this.baseUrl) as Observable<Array<Trade>>;
  }

  retrieveTradeById(id: number): Observable<Trade>{
    return this.http.get(this.baseUrl + '/' + id) as Observable<Trade>;
  }

  retrieveTradeByState(state: string): Observable<Array<Trade>>{
    return this.http.get(this.baseUrl + '?tradeState=' + state.toUpperCase()) as Observable<Array<Trade>>;
  }
}
